const btnStartRecord = document.getElementById('btnStartRecord');
const btnStopRecord = document.getElementById('btnStopRecord');
const btnPlay = document.getElementById('play');
const message = document.getElementById('message');

let recognition = new webkitSpeechRecognition();
recognition.lang = 'es-ES';
recognition.continuous = true;
recognition.interimResults = false;

recognition.onresult = (event) => {
    const results = event.results;
    const phrase = results[results.length -1][0].transcript;
    message.value = phrase;
}

recognition.onend = (event) => {
    console.log('El micrófono ha dejado de escuchar');
}

recognition.onerror = (event) => {
    console.log(event.error);
}

btnStartRecord.addEventListener('click', () => {
    recognition.start();
});

btnStopRecord.addEventListener('click', () => {
    recognition.abort();
});

btnPlay.addEventListener('click', () => {
    readMessage(message.value);
})

function readMessage(message) {
    const speech = new SpeechSynthesisUtterance();
    speech.text = 'Usted ha dicho ' + message + '. Si es incorrecto, vuelva a hablar';
    speech.volume = 1;
    speech.rate = 1;
    speech.pitch = 1;

    window.speechSynthesis.speak(speech);
    console.log(speech.text);
}