
# Web Speech API Demo

Este repositorio contiene una pequeña aplicación web con dos funcionalidades: 

- Reconocimiento de voz 
- Síntesis de voz  (texto a voz o tts)

## Probando la aplicación

Para probar la aplicación:

- Abra la consola de comandos del sistema
- Clone este repositorio
- Si usa Python 2.7 ejecute el comando: `python -m SimpleHTTPServer [puerto]`
-  Si usa Python 3 ejecute el comando: `python -m http.server [puerto]` 
- Abra el navagador Chrome y escriba en la barra de direcciones del navegador: `localhost:[puerto]`
- Haga clic en el botón **Empezar a grabar**
- El navegador le preguntará si desea permitir el acceso al micrófogo, haga clic en el botón **Permitir** del navegador
- Hable, puede decir una o más palabras. Cuando termine de hablar, guarde silencio unos segundos, luego visualizará lo que ha dicho en el textarea
- Una vez que se muestra el mensaje en el textarea, haga clic en el botón **Detener grabación**
- Para escuchar lo que ha dicho, haga clic en el botón **Reprodrucir**

![](https://i.imgur.com/V13zjQQ.png)